function [maskedNT, tracer] = loadData(dataFname)
    load(dataFname, 'maskedNT', 'tracer')
end