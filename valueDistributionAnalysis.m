idx8vals = find(idx8);
idx16vals = find(idx16);

allValuesRatio8 = [];
allValuesRatio16 = [];
allNT = [];
allTracers = [];
for i = 1:length(idx8vals)
   rawVals8 = normalizedRatio{idx8vals(i)}(:);
   rawVals16 = normalizedRatio{idx16vals(i)}(:);
   validVals8  = rawVals8(isfinite(rawVals8));
   validVals16 = rawVals16(isfinite(rawVals16));
   allValuesRatio8 = [allValuesRatio8; validVals8./length(validVals8)];
   allValuesRatio16 = [allValuesRatio16; validVals16./length(validVals16)]; 

end


% for i = 1:22
%    allNT = [allNT; aggNT{i}(:)];
%    allTracers = [allTracers; aggTracers{i}(:)];
% end

%% Generate histograms

histogram(allValuesRatio8((allValuesRatio8)>0))
hold on
histogram(allValuesRatio16((allValuesRatio16)>0))
set(gca, 'XScale','log')

%%
hHigh = hist(allValuesRatio16((allValuesRatio16)>0),1e3);
hLow = hist(allValuesRatio8((allValuesRatio8)>0),1e3);
plot(hLow);
hold on
plot(hHigh);
set(gca, 'XScale','log')
%%
pdfHigh = hHigh/sum(hHigh);
pdfLow = hLow/sum(hLow);
plot(pdfLow); hold on; plot(pdfHigh)
set(gca, 'XScale','log')