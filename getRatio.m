DATASET = 1; % 0 :NT; 1: Drug

if DATASET == 0
    dataLocation =  fullfile('data','NT');
elseif DATASET == 1
    dataLocation = fullfile('data','Inhibitor_NT');
end

load(fullfile(dataLocation,'dataList.mat'));
load(fullfile(dataLocation, 'indices.mat'));
addpath(dataLocation)
%% Parameters

saveResultsFlag = 0;
tic;
%% Process eye by eye at a given threshold value
ratio = [];
numEyes = length(dataList);
for i =1:numEyes
    
    layerRatio = getLayerRatio(dataList{i});
    %     aggNT{i} = squeeze(sum(NTlayers, 1));
    %     aggTracers{i} = squeeze(sum(tracerLayers, 1));
    %     rawRatio{i} = aggNT{i}./aggTracers{i};
    %     rawRatio{i}(~isfinite(rawRatio{i})) = 0;
    %     rawRatioThresh = autoThreshold(rawRatio{i});
    %     ratio{i} = rawRatio{i}(rawRatio{i}>rawRatioThresh);
    ratio{i} = squeeze(nansum(layerRatio, 1));
    imagesc(ratio{i})
    normalizedRatio{i} = ratio{i}./numel(ratio{i});
    fprintf('Finished processing file %d of %d\n',i,length(dataList));
end
toc
%% Normalized mean aggregated ratio across layers, i.e. cumulative along layers,
% mean of the "pixel" values of this aggregated one

for i = 1:length(dataList)
    %     imagesc(ratio{i})
    %     pause;
    normalizedRatio{i} = ratio{i};
    aux = normalizedRatio{i}(:);
    filteredNormRatio = aux(isfinite(aux));
    normAggRatio(i) = mean(filteredNormRatio);
end

% Each element of normAggRatio corresponds to the data file index stored in
% dataFname
%%


if DATASET == 0
    results = [squeeze(normAggRatio(idx8)'), squeeze(normAggRatio(idx16)')]; % results8_16
    num16above8 = sum(results(:,1) < results(:,2))
    relativeRatio = (results(:,2)./results(:,1));
elseif DATASET == 1
    results = [squeeze(normAggRatio(idxC)'), squeeze(normAggRatio(idxE)')]; % resultsC_E
    numEaboveC = sum(results(:,1) < results(:,2))
    relativeRatio = (results(:,2)./results(:,1));
end



if (saveResultsFlag)
    saveFnameStr = 'results/results_t=%d.mat';
    saveFname = sprintf(saveFnameStr, NTthresh);
    if (~exist(saveFname,'file'))
        save(saveFname, 'normAggRatio','results8_16','num16above8','relativeRatio');
    else
        save(saveFname, 'normAggRatio','results8_16','num16above8','relativeRatio','-append');
    end
end

%% Plots

% 1)

% Renormalize the results for visualization
results8  = results(:,1);
results16 = results(:,2);

normRes8  = results8./(max(results16));
normRes16 = results16./(max(results16));

xAxis = 1:length(results);

% sorting

[~, ord] = sort(results16);

figure
h1 = plot(xAxis,results8(ord),'.','MarkerSize',20);
hold on;
h2 = plot(xAxis,results16(ord),'.','MarkerSize',20);
col1 = get(h1,'Color');
col2 = get(h2,'Color');
plot(xAxis, ones(size(xAxis))*median(results8),'Color',col1),
plot(xAxis, ones(size(xAxis))*median(results16),'Color',col2),
if DATASET == 0
    legend('ratio @ 8 mmHg', 'ratio @ 16 mmHg', 'median @ 8 mmHg', 'median @ 16 mmHg')
elseif DATASET == 1
    legend('ratio in control samples', 'ratio in drug-expressed samples', 'median in control samples', 'median in drug-expressed samples')
end
xlabel('Eye pair index')
ylabel('Normalized ratio')
axis tight
set(gca,'XTickLabel',num2str(ord))
grid on

%% 2)
percentChangeRatio = (100*relativeRatio);
[~, ordPercentRatio] = sort(percentChangeRatio);
figure;
h = plot(xAxis,percentChangeRatio(ordPercentRatio),'.','MarkerSize',20);
hold on
col = get(h,'Color');
plot(1:length(results), ones(1,length(results))*mean(percentChangeRatio),'Color',col);
plot(1:length(results), ones(1,length(results))*100,'Color','k');
axis tight
legend('% change in ratio', 'mean fold increase','ratio@16=ratio@8')
ylabel('% change in ratio')
xlabel('Eye Pair index')
set(gca,'XTickLabel',num2str(ordPercentRatio));
grid on

toc