load('dataList.mat');

fidNT = fopen('NT.txt'); % file id for NT textfile

fileNames = textscan(fidNT, '%s');
fileNames = fileNames{1};

for i = 1:length(fileNames)
    fileName = fileNames{i};
    dataFname = dataList{i};
    getNTAndBgData(fileName, dataFname);
    fprintf('Finished processing file %d of %d\n',i,length(dataList));
end
