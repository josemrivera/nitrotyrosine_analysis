function layerRatio = getLayerRatio(dataFname)

   [maskedNT, tracer] = loadData(dataFname);
    numLayers = size(maskedNT,3);
    NTlayers = [];
    tracerLayers = [];
    layerRatio = [];
    for j = 1 : numLayers
        NTvals = maskedNT(:,:,j);
%         NTthresh = autoThreshold(NTvals);
        tracerVals = tracer(:,:,j);
        tracerThresh = autoThreshold(tracerVals);
%         NTvals(NTvals<=NTthresh) = 0; % this is probably the background value
        tracerVals(tracerVals<=tracerThresh) = 0;
        NTlayers(j,:,:) = NTvals;
        tracerLayers(j,:,:) = tracerVals;
        layerRatio(j,:,:) = NTvals./tracerVals;
    end
    
end