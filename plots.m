%% Histograms

% RUN valueDistributionAnalysis.m first cell first

allValuesRatio8pos = allValuesRatio8((allValuesRatio8)>0);
allValuesRatio16pos = allValuesRatio16((allValuesRatio16)>0);

figure;
hHigh = hist(allValuesRatio16pos,1e3);
hLow = hist(allValuesRatio8pos,1e3);
plot(hLow);
hold on
plot(hHigh);
set(gca, 'XScale','log')
legend('NT/tracer ratio @ 8 mmHg','NT/tracer ratio @ 16 mmHg')
title('Distribution of the normalized ratio at different pressure level')

%% Ranked Ratios

figure
h1 = plot(xAxis,results8_16(ord,1),'.','MarkerSize',20);
hold on;
h2 = plot(xAxis,results8_16(ord,2),'.','MarkerSize',20);
col1 = get(h1,'Color');
col2 = get(h2,'Color');
plot(xAxis, ones(size(xAxis))*median(results8_16(:,1)),'Color',col1),
plot(xAxis, ones(size(xAxis))*median(results8_16(:,2)),'Color',col2),
legend('ratio @ 8 mmHg', 'ratio @ 16 mmHg', 'median @ 8 mmHg', 'median @ 16 mmHg')
xlabel('Eye pair index')
ylabel('Normalized ratio')
axis tight
grid on
set(gca,'XTickLabel',num2str(ord))

%% Percentage ratio

percentChangeRatio = (100*relativeRatio);
[~, ordPercentRatio] = sort(percentChangeRatio);
figure;
h = plot(xAxis,percentChangeRatio(ordPercentRatio),'.','MarkerSize',20);
hold on
col = get(h,'Color');
plot(1:length(results8_16), ones(1,length(results8_16))*mean(percentChangeRatio),'Color',col);
plot(1:length(results8_16), ones(1,length(results8_16))*100,'Color','k');
axis tight
legend('% change in ratio', 'mean fold increase','ratio@16=ratio@8')
ylabel('% change in ratio')
xlabel('Eye Pair index')
set(gca,'XTickLabel',num2str(ordPercentRatio));
grid on

%% Boxplots
addpath('./tools/notBoxPlot')
notBoxPlot(results8_16)
%boxplot(results8_16)
set(gca,'XTickLabel',{'8 mmHg', '16 mmHg'})
title('Ratio NT/tracer at different pressure level')

%% Without outliers


addpath('./tools/notBoxPlot')
notBoxPlot(results8_16)
%boxplot(results8_16)
set(gca,'XTickLabel',{'8 mmHg', '16 mmHg'})
title('Ratio NT/tracer at different pressure level')

%%%%%%%%%%%%%%%%%%%%%%%%% EXPERIMENTAL

%% Image thresholding data acquisition journey
load('ratio_data_gen_data.mat')
figure;
imagesc(NTvals)
figure;
imagesc(tracerValsBeforeThresh)
%suprathres:
figure;
imagesc(tracerVals>0)
figure;
imagesc(squeeze(layerRatio(j,:,:)))

%% Superimposing NT over tracer
figure;
imagesc(tracerVals)
% here we need to set colormap 
hold on
imagesc(NTvals)
% here we need to change colormap

