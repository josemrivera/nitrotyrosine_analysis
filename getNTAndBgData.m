function [] = getNTAndBgData(NTfileName, dataFname)

load(dataFname); % load tracer data

imgInfo = imfinfo(NTfileName);

numLayers = length(imgInfo);
for i = 1:numLayers
    
    NT(:,:,i) = imread(NTfileName,i); % I is a variable storing a multilayer microscopy img
    maskedNT(:,:,i) = double(NT(:,:,i)).*combinedMask;
    bgNT(:,:,i) = double(NT(:,:,i)).*bgOnlyMask;
end

save(dataFname, 'maskedNT', 'bgNT','-append');
