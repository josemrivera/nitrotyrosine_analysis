function [thresh] = autoThreshold(maskedMatrix)

maskedPositiveMat = maskedMatrix(maskedMatrix>0);

[~,c] = histcounts(maskedPositiveMat,100);

if (c(1) > 1)
    thresh = ceil(c(1))+1;
else
    thresh = c(1);
end

end