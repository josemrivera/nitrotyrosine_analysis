%% Do the Tracers images

fidTracers = fopen('Tracers.txt'); % file id for NT textfile

fileNames = textscan(fidTracers, '%s');
fileNames = fileNames{1};

for i = 1:length(fileNames)
    fileName = fileNames{i};
    [dataList{i}] = getTracerAndBgData(fileName);
end

%save('dataList.mat', 'dataList');
