load('dataList.mat');
load('indices.mat');

%% Parameters

threshold = 0:15;

saveResultsFlag = 0;

%% Process eye by eye
for t = 1:length(threshold)
    
    for i =1:length(dataList)
        
        dataFnameL = dataList{i};
        load(dataFnameL);
        numLayers = size(maskedNT,3);
        ratio = [];
        NTlayers = [];
        tracerLayers = [];
        for j = 1 : numLayers
            NTvals = maskedNT(:,:,j);
%             NTthresh = autoThreshold(NTvals);
            tracerVals = tracer(:,:,j);
            %             tracerThresh = autoThreshold(tracerVals);
            NTvals(NTvals<=threshold(t)) = 0;
            tracerVals(tracerVals<=threshold(t)) = 0;
            NTlayers(j,:,:) = NTvals;
            tracerLayers(j,:,:) = tracerVals;
        end
        aggNT = sum(NTlayers, 1);
        aggTracers = sum(tracerLayers, 1);
        aux = squeeze(aggNT./aggTracers);
        ratio{i} = aux(isfinite(aux));
        normalizedRatio{i} = ratio{i}./numel(ratio{i});
        fprintf('Finished processing file %d of %d\n',i,length(dataList));
    end
    
    %% Normalized mean aggregated ratio across layers, i.e. cumulative along layers,
    % mean of the "pixel" values of this aggregated one
    
    for i = 1:length(dataList)
        normAggRatio(i) = mean(normalizedRatio{i}(:));
    end
    
    % Each element of normAggRatio corresponds to the data file index stored in
    % dataFname
    %%
    
    results8_16(t,:,:) = [squeeze(normAggRatio(idx8)'), squeeze(normAggRatio(idx16)')];
    t
    num16above8(t) = sum(results8_16(t,:,1) < results8_16(t,:,2))
    relativeRatio(t,:) = (results8_16(t,:,2)./results8_16(t,:,1));
    relativeRatio
    
    if (saveResultsFlag)
        saveFnameStr = 'results/results_t=%d.mat';
        saveFname = sprintf(saveFnameStr, t);
        if (~exist(saveFname,'file'))
            save(saveFname, 'normAggRatio','results8_16','num16above8','relativeRatio');
        else
            save(saveFname, 'normAggRatio','results8_16','num16above8','relativeRatio','-append');
        end
    end
end
