function [dataToSaveFname] = getTracerAndBgData(fileName)

% Read all the layers into I

imgInfo = imfinfo(fileName);

numLayers = length(imgInfo);
for i = 1:numLayers
    
    I(:,:,i) = imread(fileName,i); % I is a variable storing a multilayer microscopy img
    
end

%% Get the max intensity and integrated images

maxIntensityImg = max(I,[],3);
integratedImg = sum(I,3);

%% Threshold the max and integrated images

bw = maxIntensityImg > 0;
bw2 = bwareaopen(bw,10);
%imshow(bw2);

%% To create manual masks:

%% Assume 4 for now, make it adaptative in v2.0
imagesc(maxIntensityImg)
combinedMask = zeros(size(maxIntensityImg,1),size(maxIntensityImg,2));

for i =1:4
    
    
    h = impoly()
    
    mask = h.createMask;
    
    combinedMask = combinedMask + mask;
    
end

%% Construct larger mask with some background (BG) info.

numPixelsBG = 100;

se = strel('disk',numPixelsBG);

maskWithBG = imdilate(combinedMask,se);

bgOnlyMask = maskWithBG - combinedMask;


%% Get masked info from original data

for j = 1:numLayers
    
    tracer(:,:,j) = double(I(:,:,j)).* combinedMask;
    tracerBg(:,:,j) = (double(I(:,:,j)).*maskWithBG) - tracer(:,:,j);
    
end

[~, fileNameWithoutExt, ~] = fileparts(fileName);

str = textscan(fileNameWithoutExt,'%s','Delimiter','_');
dataToSaveFname = [str{1}{1} '_' str{1}{3} '.mat'];

save(dataToSaveFname, 'combinedMask','maskWithBG','bgOnlyMask','tracer','tracerBg','I');
